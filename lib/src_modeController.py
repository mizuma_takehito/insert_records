from .model1.mode1_main import mainMode1
from .model2.mode2_main import mainMode2


###################################################

def modeController(mode: str, option: dict):
  # mode1
  if   mode == "mode1":
    mainMode1(option["defaultValuesFileName"], option["targetTableNames"], option["outputFileName"])

  # mode2
  elif mode == "mode2":
    mainMode2(option["inputFileName"], option["targetTableNames"], option["valuesWithComment"])
