import itertools

from ..util.util_openpyxl import setCellValue, searchSectionTitleRowIndex, searchColumnNameRowIndex, getCellCoordinate, \
  setAutoColumnWidth, __COLUMN_INDEX__COLUMN_NAME, __COLUMN_INDEX__RECORD_FIRST, __COLUMN_INDEX__TABLE_NAME_VALUE
import openpyxl
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment


###################################################

def output(addTables: list, defaultValues: dict, outputFileName: str):
  # 出力excel
  workbook = openpyxl.Workbook()
  while len(workbook.worksheets) > 0: workbook.remove(workbook.worksheets[0]) # 全シートクリア

  # 罫線設定
  side = Side(style = 'thin', color = '000000')
  cellBorderAll = Border(top = side, bottom = side, left = side, right = side)
  # 塗りつぶし色
  cellFillHeader = PatternFill(patternType = 'solid', fgColor = 'ffff00') # yellow
  cellFillHeaderAccent = PatternFill(patternType = 'solid', fgColor = 'ffa500') # orange
  cellFillBodyAccent = PatternFill(patternType = 'solid', fgColor = 'f6f9d4') # orange
  # 文字揃え
  cellAlignmentCenter = Alignment(horizontal = 'center')
  cellAlignmentRight = Alignment(horizontal = 'right')

  addTableNames = [item['tableName'] for item in addTables]
  for addTable in addTables:
    tableName = addTable['tableName']
    tableProperties = addTable['tableProperties']
    preCondTableProperties = addTable['preCondTableProperties']
    refTableNamesAll = addTable['refTableNames']
    print(f"===================== addTable: '{tableName}' =========================")

    # テーブル情報
    print("##### テーブル情報")
    for tableProperty in tableProperties:
      print(tableProperty)

    # 前提条件となっているテーブルのとのつながり
    print("##### 前提条件となっているテーブルのとのつながり")
    for preCondTableProperty in preCondTableProperties:
      print(preCondTableProperty)

    # 前提条件となっているテーブルのとのつながり（列名 --> 前提テーブル.列名 辞書変換）
    preCondTablePropertyFromTo = dict()
    for preCondTableProperty in preCondTableProperties:
      for connection in preCondTableProperty["CONNECTION"]:
        preCondTablePropertyFromTo[connection["COLUMN_NAME"]] = {
          "REFERENCED_TABLE_NAME": preCondTableProperty["REFERENCED_TABLE_NAME"],
          "REFERENCED_COLUMN_NAME": connection["REFERENCED_COLUMN_NAME"]
        }
    for _val in preCondTablePropertyFromTo.keys(): print(f"{_val} <-- {preCondTablePropertyFromTo[_val]}")

    # おまけ情報
    print("##### おまけ情報")
    refTableNames = list(set(refTableNamesAll) - set(addTableNames)) # その他参照
    if len(refTableNames) > 0:
      # curRefTableNames = list(set(refTableNamesAll) - set(refTableNames)) # 追加済み参照
      # if len(curRefTableNames) > 0: print(f"    <<<    {curRefTableNames} 以外に")
      print(f"    <<<    {refTableNames} からも参照されています。")
    else:
      print("    <<<     その他参照なし。")

    ############################################### excel出力
    print("##### excel出力")
    # openpyxl による Excelファイル操作方法のまとめ
    # https://gammasoft.jp/support/how-to-use-openpyxl-for-excel-file/
    # シート作成
    sheet = workbook.create_sheet(title = tableName)

    rowIndex = 0

    # TODO 複数レコード追加への対応？
    recordCount = 1

    ##### テーブル情報
    # ヘッダー
    rowIndex += 1
    colIndex = 0
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "##[1]##")
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "テーブル情報")
    # ボディ
    tableInfoTableRowIndex0 = rowIndex + 1
    rowIndex += 1
    colIndex = 0
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "テーブル名", border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, tableName, border = cellBorderAll)

    ## 空白行
    rowIndex += 1

    ##### 列情報
    rowIndex += 1
    colIndex = 0
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "##[2]##")
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "列情報")
    # ヘッダー
    rowIndex += 1
    colIndex = 0
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "#", alignment = cellAlignmentRight, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "列名", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "データ・タイプ", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "NULL許容", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "自動増加", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "キー", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "DB既定値", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "Expression", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    recordNames = [f"レコード#{recordIndex + 1}" for recordIndex in range(recordCount)]
    for recordName in recordNames:
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, recordName, alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeaderAccent)

    # ボディ
    columnInfoTableRowIndex0 = rowIndex + 1
    for tableProperty in tableProperties:
      rowIndex += 1
      colIndex = 0
      columnName = tableProperty["COLUMN_NAME"]
      isNullable = tableProperty["IS_NULLABLE"] == "YES"
      isAutoIncrement = "auto_increment" in tableProperty["EXTRA"]
      columnDefault = tableProperty["COLUMN_DEFAULT"]
      # print(f"COLUMN_NAME: {COLUMN_NAME}")
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, tableProperty["INDEX"] + 1, border = cellBorderAll)
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, columnName, border = cellBorderAll)
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, tableProperty["COLUMN_TYPE"], border = cellBorderAll)
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "○" if isNullable else None, alignment = cellAlignmentCenter, border = cellBorderAll)
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "○" if isAutoIncrement else None, alignment = cellAlignmentCenter, border = cellBorderAll)
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, tableProperty["COLUMN_KEY"], border = cellBorderAll)
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, columnDefault, border = cellBorderAll)
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, tableProperty["EXTRA"], border = cellBorderAll)

      for recordIndex in range(recordCount):
        # [挿入する値]　（上から優先的に適用。）
        # 1. 前提条件となっているテーブルのとのつながりがあればシート間で値参照。
        # 2. 外部入力ファイルからのデフォルト値。
        # 3. DB既定のデフォルト値。 ---> 廃止
        # 4. auto_incrementなら 1からの連番。
        # 5. NULL許容なら'NULL' (NULL許容でも参照テーブルがあれば#1で処理される)
        colIndex += 1
        connection = preCondTablePropertyFromTo.get(columnName)
        defaultValuesByTableName = defaultValues.get(tableName)
        defaultValueByColumnName = defaultValuesByTableName.get(columnName) if defaultValuesByTableName is not None else None
        insertValue = None # 挿入する値
        # 1. 前提条件となっているテーブルのとのつながりがあればシート間で値参照。
        if connection is not None:
          # print(connection)
          REFERENCED_TABLE_NAME = connection["REFERENCED_TABLE_NAME"]
          REFERENCED_COLUMN_NAME = connection["REFERENCED_COLUMN_NAME"]

          # 前提テーブル.列名　セル検索
          refSheet = workbook[REFERENCED_TABLE_NAME]
          columnInfoRowIndex = searchSectionTitleRowIndex(refSheet, 2) # セクション検索
          # print(f"columnInfoRowIndex = {columnInfoRowIndex}")
          if columnInfoRowIndex > 0:
            ColumnNameRowIndex = searchColumnNameRowIndex(refSheet, columnInfoRowIndex + 2, REFERENCED_COLUMN_NAME) # 列検索
            print(f"[{REFERENCED_TABLE_NAME}].{refSheet[getCellCoordinate(sheet, ColumnNameRowIndex, 2)].value}　の値を参照。")
            insertValue = str(f"={REFERENCED_TABLE_NAME}!{getCellCoordinate(sheet, ColumnNameRowIndex, colIndex)}")
        # 2. 外部入力ファイルからのデフォルト値。
        elif defaultValueByColumnName is not None:
          insertValue = defaultValueByColumnName
        # 3. DB既定のデフォルト値。int、float以外全てstring ---> 廃止
        # elif columnDefault is not None:
        #   # noinspection PyBroadException
        #   try: columnDefault = float(columnDefault) # float変換
        #   except:
        #     # noinspection PyBroadException
        #     try: columnDefault = int(columnDefault) # int変換
        #     except:
        #       columnDefault = f"'{str(columnDefault)}'" # それ以外全てstring扱いでシングルクォーテーションでくくる
        #   insertValue = columnDefault
        # 4. auto_incrementなら レコード番号。
        elif isAutoIncrement:
          insertValue = recordIndex + 1
        # 5. NULL許容なら'NULL' (NULL許容でも参照テーブルがあれば#1で処理される)
        elif isNullable:
          insertValue = 'NULL'
        # 挿入
        # print("insertValue: " + str(insertValue))
        setCellValue(sheet, rowIndex, colIndex, insertValue, border = cellBorderAll, fill = cellFillBodyAccent)
        sheet.cell(rowIndex, colIndex, insertValue)

    ## 空白行
    rowIndex += 1

    ##### おまけ情報
    # ヘッダー
    rowIndex += 1
    colIndex = 0
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "##[3]##")
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "おまけ情報")

    ## 他テーブルからの参照
    rowIndex += 1
    colIndex = 0
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "他Tableから参照", border=cellBorderAll, fill=cellFillHeader)
    colIndex += 1
    refTableNames = list(set(refTableNamesAll) - set(addTableNames)) # その他参照
    if len(refTableNames) > 0:
      for refTableName in refTableNames:
        setCellValue(sheet, rowIndex, colIndex, refTableName, border=cellBorderAll)
        rowIndex += 1
    else:
      setCellValue(sheet, rowIndex, colIndex, "参照なし", border=cellBorderAll)
      rowIndex += 1

    # シート内全ての列幅を自動調整
    setAutoColumnWidth(sheet)

    # Excel関数でMySQL参考値出力
    # ヘッダー
    rowIndex += 1
    colIndex = 0
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "##[4]##")
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "出力されるMySQL文(参考値)")
    # ボディ
    rowIndex += 1
    colIndex = 0
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "#", alignment = cellAlignmentRight, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "レコード名", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    colIndex += 1; setCellValue(sheet, rowIndex, colIndex, "MySQL文(参考値)", alignment = cellAlignmentCenter, border = cellBorderAll, fill = cellFillHeader)
    for recordIndex in range(len(recordNames)):
      print(f"------- {recordIndex} -------")
      rowIndex += 1
      colIndex = 0

      nProperty = len(tableProperties) # 列数

      # テーブル名
      tableNameCell = getCellCoordinate(sheet, tableInfoTableRowIndex0, __COLUMN_INDEX__TABLE_NAME_VALUE, True, True)

      # 番号
      numberColumnIndex = colIndex + 1
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, recordIndex + 1, border=cellBorderAll) # 番号

      # 列情報テーブル左上(0,0)のセル座標　($A$6)
      baseCell = getCellCoordinate(sheet, columnInfoTableRowIndex0, 1, True, True)
      print(baseCell)

      # レコード番号による列オフセットを示すセル座標　($A22)
      columnOffsetCell = getCellCoordinate(sheet, rowIndex, numberColumnIndex, _isLockColumn=True)
      print(f"columnOffsetCell = {columnOffsetCell}")

      # 列名　開始セル座標　終了セル座標　[$B$6:$B$15]
      columnNameCells = list()
      for iProperty in range(nProperty):
        columnNameCells.append(getCellCoordinate(sheet, columnInfoTableRowIndex0 + iProperty, __COLUMN_INDEX__COLUMN_NAME, True, True))
      # columnNameCellBegin = columnNameCells[0]
      # columnNameCellEnd = columnNameCells[nProperty - 1]
      # print(f"columnNameCells: {str(columnNameCells)}")
      # print(f"columnNameCell = [{columnNameCellBegin}:{columnNameCellEnd}]")

      # このレコードの挿入する値　開始セル座標　終了セル座標　OFFSET($A$6,0,8+$A22-1)、OFFSET($A$6,9,8+$A22-1)
      insertCells = list()
      for iProperty in range(nProperty):
        insertCells.append(f"OFFSET({baseCell},{iProperty},{__COLUMN_INDEX__RECORD_FIRST - 1}+{columnOffsetCell}-1)")
      # insertCellBegin = insertCells[0]
      # insertCellEnd = insertCells[nProperty - 1]
      # print(f"insertCells: {str(insertCells)}")
      # print(f"insertCell = [{insertCellBegin}:{insertCellEnd}]")

      # レコード名
      recordNameCell = f"=OFFSET({baseCell},-1,{__COLUMN_INDEX__RECORD_FIRST - 1}+{columnOffsetCell}-1)"
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, recordNameCell, border=cellBorderAll)

      # MySQL文　旧VerのExcelではTEXTJOINが使えないのでCONCATENATEで代用
      columnNamesConcatenate = f"CONCATENATE({','.join(columnNameCells)})"
      print(columnNamesConcatenate)
      insertCellsSeps = ['","' for _ in range(len(insertCells) - 1)]; insertCellsSeps.append("")
      insertCellsZip = list(itertools.chain.from_iterable([i for i in zip(insertCells, insertCellsSeps)])); insertCellsZip.pop()
      insertCellsConcatenate = f"CONCATENATE({','.join(insertCellsZip)})"
      print(insertCellsConcatenate)
      command = f'="INSERT INTO `"&{tableNameCell}&"` ("&{columnNamesConcatenate}&") VALUES ("&{insertCellsConcatenate}&")"'
      print(command)
      colIndex += 1; setCellValue(sheet, rowIndex, colIndex, command, border=cellBorderAll)

  # Excel保存
  workbook.save(outputFileName)
