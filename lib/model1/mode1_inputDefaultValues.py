from ..util.util_openpyxl import getTableData
import openpyxl


###################################################

def inputDefaultValues(defaultValuesFileName: str):
  defaultValues = dict()

  if defaultValuesFileName is not None:
    try:
      # 入力excel(シート１)
      workbook = openpyxl.load_workbook(defaultValuesFileName, read_only = True, data_only = True)
      for sheet in workbook.worksheets:
        tableData = getTableData(sheet)
        tableInfo = tableData["tableInfo"]
        records = tableData["records"]
        # print(tableInfo)
        # for record in records: print(record)

        # レコード#1に記入している内容を、ユーザー指定の既定値として扱うこととする。
        if len(records.items()) == 0: print("デフォルト値を設定するためのレコード#1がありません。"); exit(1)
        recordsFirstKey = next(iter(records))
        recordsFirstItem = records[recordsFirstKey]
        defaultValuesRecordName = recordsFirstItem["recordName"]
        defaultValuesRecordData = recordsFirstItem["columnInfos"]
        print(f"defaultValuesRecordName = {defaultValuesRecordName}")
        # print(defaultValuesRecordData)

        tableName = tableInfo["tableName"]
        # print("tableName = " + tableName)
        defaultValuesInTable = dict() # このテーブルに設定したいデフォルト値
        for columnInfo in defaultValuesRecordData:
          # print(columnInfo)
          columnName = columnInfo["columnName"]
          defaultValue = columnInfo["insertValue"]
          if defaultValue is None: continue # 空欄は無視
          print(f"[{tableName}].{columnName} --> {defaultValue}")
          defaultValuesInTable[columnName] = defaultValue
        defaultValues[tableName] = defaultValuesInTable
        # print(defaultValues)

      # 後処理
      workbook.close()

    except FileNotFoundError:
      print(f"ファイルが見つかりません： '{defaultValuesFileName}'")
      exit(1)

  # print(defaultValues)
  # exit(1)
  return defaultValues
