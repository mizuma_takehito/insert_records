from ..util.util_mysql import curExecute, getCursorMySQL
from sqlite3 import Cursor
import itertools
from ..util.util_mysql import getAllTableNames


###################################################

# レコード追加の前提となるテーブルについて処理。（ターゲットとなるテーブルも含む）
def makeAddTablesChild(cur: Cursor, targetTableNames: list, curTableName: str, addTables: list):
  if curTableName in [item['tableName'] for item in addTables]:
    # print("既に追加されているテーブルなので省略します。")
    return

  print(f"\n##### {curTableName}追加するために前提必須の参照テーブルを検索。循環参照の場合を除く。")
  curExecute(cur,
             f"SELECT REFERENCED_TABLE_NAME, "
             f"GROUP_CONCAT(COLUMN_NAME SEPARATOR '|'), "
             f"GROUP_CONCAT(REFERENCED_COLUMN_NAME SEPARATOR '|') "
             # f"GROUP_CONCAT(POSITION_IN_UNIQUE_CONSTRAINT SEPARATOR '|')"
             f"FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE "
             f"WHERE TABLE_NAME = '{curTableName}' AND REFERENCED_TABLE_NAME IS NOT NULL "
             f"GROUP BY REFERENCED_TABLE_NAME",
             True)
  rows = cur.fetchall()
  # for row in rows: print(row)
  preCondTableProperties = list()
  for tableProperty in rows:
    preCondTableProperties.append({
      "REFERENCED_TABLE_NAME": tableProperty[0], # 前提テーブル名
      "CONNECTION": [{ # 接続する列に関する情報
        "COLUMN_NAME": refColumn[0], # 自分のテーブルの列名
        "REFERENCED_COLUMN_NAME": refColumn[1], # 接続先の列名
        # "POSITION_IN_UNIQUE_CONSTRAINT": refColumn[2], # 接続先の列インデックス
      } for refColumn in list(zip(
        tableProperty[1].split('|'),
        tableProperty[2].split('|'),
        # tableProperty[3].split('|'),
      ))],
    })
  print("前提必須の参照テーブル情報:")
  for preCondTableProperty in preCondTableProperties: print(preCondTableProperty)
  print("---------")

  # NULL許容の列名リスト作成
  curExecute(cur,
             f"SELECT COLUMN_NAME "
             f"FROM INFORMATION_SCHEMA.`COLUMNS` c "
             f"WHERE TABLE_NAME = '{curTableName}' AND c.IS_NULLABLE = 'YES'",
             True)
  nullableColumnNames = list(itertools.chain.from_iterable(cur.fetchall()))
  print(f"NULL許容の列名リスト --> {nullableColumnNames}")
  print("---------")

  # 参照テーブル追加が未要求の場合、かつ、列名がNULL許容の場合、"CONNECTION"から列名を削除
  print("参照テーブル追加が未要求の場合、かつ、列名がNULL許容の場合、'CONNECTION'から列名を削除")
  for preCondTableProperty in preCondTableProperties:
    refTableName = preCondTableProperty["REFERENCED_TABLE_NAME"]
    connections = preCondTableProperty["CONNECTION"]
    print(f"----- {refTableName}")
    print(connections)

    # 参照テーブル追加要求があればスキップ
    if refTableName in targetTableNames:
      print(f"[{refTableName}]は追加要求があるため、NULL許容チェックをスキップします。")
      continue

    # NULL許容の列名リストを削除
    preCondTableProperty["CONNECTION"] = [connection for connection in connections if not connection["COLUMN_NAME"] in nullableColumnNames]

  # preCondTableProperties[1]["CONNECTION"] = [] # テスト
  print("---------")

  # プロパティがなくなったテーブルを削除
  preCondTableProperties = [val for val in preCondTableProperties if len(val["CONNECTION"]) > 0]
  print("NULL許容の列を削除し、プロパティがなくなったテーブルを削除後:")
  for preCondTableProperty in preCondTableProperties: print(preCondTableProperty)
  print("---------")

  # 前提として追加が必要なテーブルに再帰
  REFERENCED_TABLE_NAMEs = [preCondTableProperty['REFERENCED_TABLE_NAME'] for preCondTableProperty in preCondTableProperties]
  preCondTableNames = [tableName for tableName in REFERENCED_TABLE_NAMEs if tableName != curTableName] # 再帰ループを除外
  for preCondTableName in preCondTableNames:
    print(f"{preCondTableName} が前提として必要なので再帰ループします ---------------")
    makeAddTablesChild(cur, targetTableNames, preCondTableName, addTables)

  # 現在見てるテーブル情報収集
  curExecute(cur, f"SHOW COLUMNS FROM `{curTableName}`", True)
  rows = cur.fetchall()
  # for row in rows: print(row)
  tableProperties = list()
  for row in rows:
    tableProperties.append({
      "INDEX": len(tableProperties),
      "COLUMN_NAME": row[0], # 列名
      "COLUMN_TYPE": row[1], # データタイプ
      "IS_NULLABLE": row[2], # Null許容ならYES
      "COLUMN_KEY": row[3], # キー: PRI, UNI, MUL
      "COLUMN_DEFAULT": row[4], # デフォルト値
      "EXTRA": row[5], # auto_increment
    })
  # for tableProperty in tableProperties: print(tableProperty)

  # 関連テーブル名の取得。
  curExecute(cur,
             f"SELECT DISTINCT(TABLE_NAME) FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE "
             f"WHERE REFERENCED_TABLE_NAME = '{curTableName}'/* AND REFERENCED_TABLE_NAME IS NOT NULL*/",
             True)
  refTableNames = list(itertools.chain.from_iterable(cur.fetchall()))

  # 追加
  addTables.append({
    "tableName": curTableName, # 基準となるテーブル名
    "tableProperties": tableProperties, # テーブル情報
    "preCondTableProperties": preCondTableProperties, # 前提となっているテーブルとの関係
    "refTableNames": refTableNames, # 基準テーブルを参照しているテーブルとプロパティのリスト
  })

# テーブルコネクション情報収集。追加が必要な順に格納。
def makeAddTables(targetTableNames: list):
  cur = getCursorMySQL()

  # テーブル指定が無ければ全てのテーブルを出力
  if targetTableNames is None:
    targetTableNames = getAllTableNames(cur)

  # 追加済みのテーブルのコネクション情報を記憶。追加が必要な順に並ぶ。
  addTables = list()
  for targetTableName in targetTableNames:
    makeAddTablesChild(cur, targetTableNames, targetTableName, addTables)

  cur.close()

  return addTables
