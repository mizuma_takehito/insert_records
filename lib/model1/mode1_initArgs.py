

###################################################

__MODEL1_OPTION_DEFAULT__OUTPUT_FILENAME = 'insertRecords.xlsx'

###################################################

# help mode1
def printUsageMode1():
  print("===== mode1 モード =====")
  print("Usage: ", "python3", "insertRecords.py", "mode1", "[OPTION]", "<TableNames>")
  print("-----------------------")
  print("<TableNames> : 以下いずれかの形式で記述する。")
  print("  --all : 全てのテーブルに関する情報を出力する。")
  print("  <TableName1> [<TableName2> [<TableName3> ..] ] :")
  print("          最低限情報出力したいテーブル名を列挙する。")
  print("-----------------------")
  print("[OPTION]")
  print(f"  -o  <filename> : テーブル情報出力Excelファイルの指定。 (既定値: '{__MODEL1_OPTION_DEFAULT__OUTPUT_FILENAME}')")
  print(f"  -d  <filename>  : 初期値設定用Excelファイルの指定。")
  print("==================================================================================")

# initialize args mode1
def initArgsMode1(argc: int, argv: list):
  # オプション整理
  option = {
    "targetTableNames": None, # 指定があれば列挙。無ければ全テーブルになる。
    "outputFileName": __MODEL1_OPTION_DEFAULT__OUTPUT_FILENAME,
    "defaultValuesFileName": None,
  }
  argIdx = 2 # 0: insertRecords.py, 1: mode1
  for _ in range(1, argc):
    if argIdx < argc:
      # print(argv[argIdx])
      if   argv[argIdx] == "-o" : option["outputFileName"] = argv[argIdx + 1]; argIdx += 2
      elif argv[argIdx] == "-d" : option["defaultValuesFileName"] = argv[argIdx + 1]; argIdx += 2

  # 必須整理
  if argIdx == argc: exit(1) # 必須が無い
  for i in range(argIdx, argc):
    if argIdx < argc:
      # print(argv[argIdx])
      # テーブル名
      if argv[argIdx] == "--all":
        argIdx += 1
      else:
        option["targetTableNames"] = argv[argIdx::]
        argIdx = argc

  if argIdx != argc: exit(1)
  return option
