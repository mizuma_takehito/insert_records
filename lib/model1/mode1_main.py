from .mode1_inputDefaultValues import inputDefaultValues
from .mode1_makeAddTables import makeAddTables
from .mode1_output import output


###################################################


def mainMode1(defaultValuesFileName: str, targetTableNames: list, outputFileName: str):
  print(f"mainMode1({defaultValuesFileName}, {targetTableNames}, {outputFileName})")

  # 初期値設定があれば取得
  print("\n##### 初期値設定があれば取得")
  defaultValues = inputDefaultValues(defaultValuesFileName)

  # テーブルコネクション情報収集
  print("\n##### テーブルコネクション情報収集")
  addTables = makeAddTables(targetTableNames)

  # テーブル情報出力
  print("\n##### テーブル情報出力")
  output(addTables, defaultValues, outputFileName)
