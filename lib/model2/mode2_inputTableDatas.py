from ..util.util_openpyxl import getTableData
import openpyxl


###################################################

# 全テーブルデータをExcelから取得する。取得するテーブルに指定があればtargetTableNamesに列挙。
def inputTableDatas(defaultValuesFileName: str, targetTableNames: list = None):
  print(targetTableNames)

  # 全テーブルデータ
  tableDataAll = list()

  try:
    # 入力excel(シート１)
    # data_only = Trueにしても値が取得できない場合、一度Excelを開いてExcel2007-365形式で保存し直すと成功するかもしれない。
    workbook = openpyxl.load_workbook(defaultValuesFileName, read_only=True, data_only=True)

    for sheet in workbook.worksheets:
      # 取得するテーブルに指定があれば、シート名が含まれるか調べる。
      tableName = sheet.title
      if targetTableNames is not None:
        if not tableName in targetTableNames:
          continue

      tableData = getTableData(sheet)
      print(tableData)
      tableDataAll.append(tableData)

    # 後処理
    workbook.close()

  except FileNotFoundError:
    print(f"ファイルが見つかりません： '{defaultValuesFileName}'")
    exit(1)

  return tableDataAll
