

###################################################

__MODEL2_OPTION_DEFAULT__OUTPUT_FILENAME = 'insertRecords.xlsx'

###################################################

# help mode2
def printUsageMode2():
  print("===== mode2 モード =====")
  print("Usage: ", "python3", "insertRecords.py", "mode2", "[OPTION]", "<TableNames>")
  print("-----------------------")
  print("<TableNames> : 以下いずれかの形式で記述する。")
  print("  --all : 全てのテーブルに関する情報を、入力したExcelのシート順に出力する。")
  print("  <TableName1> [<TableName2> [<TableName3> ..] ] :")
  print("          出力するテーブル名(入力したExcelの各シート名)を列挙。")
  print("          このリストに含まれないシートは出力されない。")
  print("-----------------------")
  print("[OPTION]")
  print(f"  -i  <filename>        : テーブル情報入力Excelファイルの指定。 (既定値: '{__MODEL2_OPTION_DEFAULT__OUTPUT_FILENAME}')")
  print(f"  --values-with-comment : 生成されるINSERT文の各VALUEに列名をコメントアウトで入れる。 (既定値: False)")
  print(f"                          (例) INSERT `テーブル名` (列名1, 列名2) VALUES (値1, 値2)")
  print(f"                           --> INSERT `テーブル名` (列名1, 列名2) VALUES (/*列名1*/値1, /*列名2*/値2)")
  print("==================================================================================")

# initialize args mode2
def initArgsMode2(argc: int, argv: list):
  # オプション整理
  option = {
    "inputFileName": __MODEL2_OPTION_DEFAULT__OUTPUT_FILENAME,
    "targetTableNames": None, # あれば列挙
    "valuesWithComment": False,
  }
  argIdx = 2 # 0: insertRecords.py, 1: mode2
  for _ in range(1, argc):
    if argIdx < argc:
      # print(argv[argIdx])
      if   argv[argIdx] == "-i" : option["inputFileName"] = argv[argIdx + 1]; argIdx += 2
      elif argv[argIdx] == "--values-with-comment" : option["valuesWithComment"] = True; argIdx += 1

  # 必須整理
  if argIdx == argc: exit(1) # 必須が無い
  isAllTargetTableNames = False
  for i in range(argIdx, argc):
    if argIdx < argc:
      # print(argv[argIdx])
      # テーブル名
      if argv[argIdx] == "--all":
        isAllTargetTableNames = True
        argIdx += 1
      elif not isAllTargetTableNames:
        option["targetTableNames"] = argv[argIdx::]
        argIdx = argc

  if argIdx != argc: exit(1)
  return option
