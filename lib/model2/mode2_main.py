from .mode2_inputTableDatas import inputTableDatas
from .mode2_output import output


###################################################

def mainMode2(inputFileName: str, targetTableNames: list, valuesWithComment: bool):
  print(f"mainMode2({inputFileName})")

  # テーブルデータの取得
  print("\n##### テーブルデータの取得")
  tableDataAll = inputTableDatas(inputFileName, targetTableNames)

  # MySQL文生成&出力
  print("\n##### MySQL文生成")
  output(tableDataAll, valuesWithComment)
