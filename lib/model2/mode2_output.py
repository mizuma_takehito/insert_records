from ..util.util_mysql import createCommandToInsertRecordsForMySQLFromTableData


###################################################

def output(tableDataAll: list, valuesWithComment: bool):
  commands = list()
  errors = list()
  for tableData in tableDataAll: # テーブルループ
    result = createCommandToInsertRecordsForMySQLFromTableData(tableData, valuesWithComment)
    for command in list(result["commands"]): commands.append(command)
    for error in list(result["errors"]): errors.append(error)

  # MySQL文出力
  print("\n===== MySQL文出力 =====")
  for command in commands:
    print(command)

  # エラー一覧
  if len(errors) > 0:
    print("\n===== エラーがあります =====")
    for error in errors: print(error)

  # 情報
  print("\n===== info. =====")
  print(f"{len(commands)} commands.")
  print(f"{len(errors)} errors.")
  if len(errors) > 0: print("** もし記入しているのにempty扱いされるならば、ファイルを一度開いてExcel2007-365形式で保存し直してみてください。")
