import configparser


###################################################

__UTIL_INI_CONFIG_FILENAME = 'insertRecords.ini'

# .iniのIOのためのconfigを取得する
def getConfigObj():
  config = configparser.ConfigParser()
  config.read(__UTIL_INI_CONFIG_FILENAME)
  return config

# MySQL_DB に関するiniを全て取得する
def iniGetMySQLDB():
  config = getConfigObj()

  section = "MySQL_DB"

  return {
    "host": config.get(section, "host"),
    "port": config.getint(section, "port"),
    "db": config.get(section, "db"),
    "user": config.get(section, "user"),
    "passwd": config.get(section, "passwd"),
    "charset": config.get(section, "charset"),
  }
