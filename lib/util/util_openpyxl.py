from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.styles.borders import Border
from openpyxl.styles import PatternFill
from openpyxl.styles.alignment import Alignment


###################################################

__SEARCH_ROW_MAX = 30  # 検索する最大行数
__SEARCH_COLUMN_MAX = 30  # 検索する最大列数

__COLUMN_INDEX__TABLE_NAME_VALUE = 2 # テーブル名　値　列番号

__COLUMN_INDEX__SECTION = 1 # セクションを判断する列番号

__SECTION2_TABLE_HEADER_HEIGHT = 1 # セクション2の表ヘッダーの高さ

__COLUMN_INDEX__NUMBER = 1 # 番号　列番号
__COLUMN_INDEX__COLUMN_NAME = 2 # 列名　列番号
__COLUMN_INDEX__DATA_TYPE = 3 # データ・タイプ　列番号
__COLUMN_INDEX__NULLABLE = 4 # NULL許容　列番号
__COLUMN_INDEX__AUTO_INCREMENT = 5 # 自動増加　列番号
__COLUMN_INDEX__KEY = 6 # キー　列番号
__COLUMN_INDEX__DEFAULT_VALUE = 7 # デフォルト値　列番号
__COLUMN_INDEX__EXPRESSION = 8 # Expression　列番号
__COLUMN_INDEX__RECORD_FIRST = 9 # レコード#1　列番号（これより右はレコード#2, #3, ..）

###################################################

# セル座標名を取得
def getCellCoordinate(sheet: Worksheet, _rowIndex: int, _colIndex: int, _isLockRow: bool = False, _isLockColumn: bool = False):
  coordinate = ("$" if _isLockColumn else "") + sheet.cell(row = 1, column = _colIndex).column_letter + ("$" if _isLockRow else "") + str(_rowIndex)
  # print(f"getCellCoordinate({_rowIndex}, {_colIndex}) --> {coordinate}")
  return coordinate

# 指定シート内の指定セルに値代入
def setCellValue(sheet: Worksheet, _rowIndex: int, _colIndex: int, _value: any,
                 alignment: Alignment = None, border: Border = None, fill: PatternFill = None):
  cell = sheet[getCellCoordinate(sheet, _rowIndex, _colIndex)]
  if cell is None:
    print(f"セル({_rowIndex}, {_colIndex})の取得に失敗しました。")
    return

  # 値代入
  cell.value = _value

  # 文字揃え
  if alignment is not None: cell.alignment = alignment

  # 罫線
  if border is not None: cell.border = border

  # 塗りつぶし色
  if fill is not None: cell.fill = fill

# 指定シート内のセクション行を検索
def searchSectionTitleRowIndex(sheet: Worksheet, sectionNumber: int):
  # print(sheet.title)
  SEARCH_ROW_MIN = 1 # 検索する最小行番号
  SEARCH_ROW_MAX = 100 # 検索する最大行番号
  _rowIndex = 0
  for row in sheet[f"{getCellCoordinate(sheet, SEARCH_ROW_MIN, __COLUMN_INDEX__SECTION)}:" \
                    f"{getCellCoordinate(sheet, SEARCH_ROW_MAX, __COLUMN_INDEX__SECTION)}"]:
    _rowIndex += 1
    for col in row:
      colValue = col.value
      if colValue is not None:
        colValue = str(colValue)
        # print(colValue)
        if colValue == f"##[{sectionNumber}]##":
          return _rowIndex
  return -1

# 指定シート、指定オフセット行以下で'列名'を検索し、該当行を返す。
def searchColumnNameRowIndex(_sheet: Worksheet, _offset: int, _columnName: str):
  _rowIndex = -1
  for row in _sheet[f"{getCellCoordinate(_sheet, _offset, __COLUMN_INDEX__COLUMN_NAME)}:" \
                    f"{getCellCoordinate(_sheet, _offset + __SEARCH_ROW_MAX, __COLUMN_INDEX__COLUMN_NAME)}"]:
    _rowIndex += 1
    for col in row:
      colValue = col.value
      if colValue is None: return -1
      colValue = str(colValue)
      # print(colValue)
      if colValue == _columnName:
        return _offset + _rowIndex
  return -1

# 指定シートのテーブル情報を取得する
def getTableData(sheet: Worksheet):
  # テーブル情報
  tableInfo = dict()
  tableInfo["tableName"] = sheet.title

  # レコード情報
  records = dict()
  columnInfoRowIndex = searchSectionTitleRowIndex(sheet, 2)  # セクション検索
  if columnInfoRowIndex > 0:
    # 列情報をレコード数だけ収集
    for recordIndex in range(__SEARCH_COLUMN_MAX): # 登録されているレコードを全て取得するためのループ
      # 列情報のデータ0行目の行インデックス
      columnInfoDataRowIndexFirst = columnInfoRowIndex + 1 + __SECTION2_TABLE_HEADER_HEIGHT

      # レコードセル(のヘッダーセル)が無ければ終わり
      recordHeaderCell = sheet.cell(row = columnInfoDataRowIndexFirst - 1, column = __COLUMN_INDEX__RECORD_FIRST + recordIndex)
      if recordHeaderCell is None or recordHeaderCell.value is None: break
      recordName = recordHeaderCell.value

      # 追加するレコード情報作成
      columnInfos = list()
      for rowIndex in range(columnInfoDataRowIndexFirst, columnInfoDataRowIndexFirst + __SEARCH_ROW_MAX):
        numberCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__NUMBER) # 番号
        columnNameCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__COLUMN_NAME)  # 列名
        dataTypeCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__DATA_TYPE)  # データ・タイプ
        nullableCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__NULLABLE) # NULL許容
        autoIncrementCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__AUTO_INCREMENT) # 自動増加
        keyCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__KEY) # キー
        defaultValueCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__DEFAULT_VALUE) # デフォルト値
        expressionCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__EXPRESSION) # Expression
        insertValueCell = sheet.cell(row = rowIndex, column = __COLUMN_INDEX__RECORD_FIRST + recordIndex) # 挿入する値

        # 処理上、無いと困る。行が空いたら収集forループ終了扱いとする。
        if numberCell is None or numberCell.value is None: break
        if columnNameCell is None or columnNameCell.value is None: break

        columnInfos.append({
          "number": numberCell.value,
          "columnName": columnNameCell.value,
          "dataType": dataTypeCell.value if dataTypeCell is not None else None,
          "nullable": nullableCell.value == "○" if nullableCell is not None else False,
          "autoIncrement": autoIncrementCell.value == "○" if autoIncrementCell is not None else False,
          "key": keyCell.value if keyCell is not None else None,
          "defaultValue": defaultValueCell.value if defaultValueCell is not None else None,
          "expression": expressionCell.value if expressionCell is not None else None,
          "insertValue": insertValueCell.value if insertValueCell is not None else None,
        })
      records[recordIndex] = {
        'recordName': recordName,
        'columnInfos': columnInfos,
      }

  tableData = {
    "tableInfo": tableInfo,
    "records": records,
  }
  return tableData

# 指定シートの列幅を全て自動調整する
def setAutoColumnWidth(sheet: Worksheet):
  for col in sheet.columns:
    max_length = 0
    column = col[0].column  # Get the column name
    column = chr(ord("A") - 1 + column)
    for cell in col:
      # noinspection PyBroadException
      try:  # Necessary to avoid error on empty cells
        if len(str(cell.value)) > max_length:
          max_length = len(cell.value)
      except:
        pass # 何もしない
    adjusted_width = max_length + 5
    sheet.column_dimensions[column].width = adjusted_width
