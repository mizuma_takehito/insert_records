from ..util.util_ini import iniGetMySQLDB
import itertools
import MySQLdb
from sqlite3 import Cursor

###################################################

# MySQL接続
def getCursorMySQL():
  configMySQLDB = iniGetMySQLDB()
  # print(configMySQLDB)
  host = configMySQLDB["host"]; print(f"host = {host}")
  port = int(configMySQLDB["port"]); print(f"port = {port}")
  db = configMySQLDB["db"]; print(f"db = {db}")
  user = configMySQLDB["user"]; print(f"user = {user}")
  passwd = configMySQLDB["passwd"]; print(f"passwd = {passwd}")
  charset = configMySQLDB["charset"]; print(f"charset = {charset}")
  conn = MySQLdb.connect(
    host = host,
    port = port,
    db = db,
    user = user,
    passwd = passwd,
    charset = charset,
  )
  return conn.cursor()

# SQLコマンド実行
def curExecute(cur: Cursor, command, isPrintCommand = False):
  cur.execute(command)
  if isPrintCommand: print(command)

# 全てのテーブル名を取得する
def getAllTableNames(cur: Cursor):
  curExecute(cur, "SHOW TABLES")
  return list(itertools.chain.from_iterable(cur.fetchall()))

# テーブル情報からMySQL文を生成する
def createCommandToInsertRecordsForMySQLFromTableData(tableData: dict, valuesWithComment: bool):
  tableInfo = tableData["tableInfo"]
  records = dict(tableData["records"])
  tableName = tableInfo["tableName"]
  # print(f"============ {tableName} ============")

  commands = list() # MySQL文。レコードの数だけ登録される。
  errors = list() # エラーがあれば登録。
  for recordIndex, record in records.items():  # レコードループ
    recordName = record["recordName"]
    columnInfos = record["columnInfos"]
    # print(f"----- record[{recordIndex}]: {recordName} -----")

    columnNames = list()  # 挿入先となる列名一覧
    values = list()  # VALUESに入れるコマンド一覧
    errorColumns = list() # エラー列名一覧。
    for columnInfo in columnInfos:  # 列情報ループ
      # print(columnInfo)
      columnName = columnInfo["columnName"]
      nullable = columnInfo["nullable"]
      insertValue = columnInfo["insertValue"]

      # NULL許容でないのに空欄があったらエラー
      if not nullable and insertValue is None:
        errorColumns.append(columnName)

      # 追加
      columnNames.append(columnName)
      comment = f"/*{columnName}*/" if valuesWithComment else ""
      values.append(f"{comment}{insertValue}")

    # コマンド生成
    columnNamesStr = ', '.join(columnNames)
    valuesStr = ', '.join(values)
    commands.append(f"INSERT INTO `{tableName}` ({columnNamesStr}) VALUES ({valuesStr})")

    # エラーがあればメモ
    if len(errorColumns) > 0:
      errors.append(f"`{tableName}` ({', '.join(errorColumns)}) is empty. (in '{recordName}')")

  return {
    "commands": commands,
    "errors": errors,
  }
