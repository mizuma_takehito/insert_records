from .model1.mode1_initArgs import printUsageMode1, initArgsMode1
from .model2.mode2_initArgs import printUsageMode2, initArgsMode2
import sys


###################################################

# help
def printUsage():
  print("==================================================================================")
  print("Usage: ", "python3", "insertRecords.py", "<mode>", "[Args]")
  print("==================================================================================")
  print("<mode> : 実行モード。以下から指定。")
  print("  mode1 : レコード追加に必要なテーブル情報を出力するモード。")
  print("  mode2 : mode1で出力されたファイルを編集し、その編集されたファイルを元にSQL文を生成するモード。")
  print("==================================================================================")
  printUsageMode1()
  printUsageMode2()

# args
def initArgs():
  # 最低限の引数長確認
  argv = sys.argv
  argc = len(argv)
  if argc < 2: printUsage(); exit(1)

  # 実行モードの取得
  mode = ""
  option = {}
  if   argv[1] == "mode1":
    mode = "mode1"
    # noinspection PyBroadException
    try: option = initArgsMode1(argc, argv)
    except: printUsageMode1(); exit(1)
  elif argv[1] == "mode2":
    mode = "mode2"
    # noinspection PyBroadException
    try: option = initArgsMode2(argc, argv)
    except: printUsageMode2(); exit(1)
  else: printUsage(); exit(1)

  args = {
    "mode": mode,
    "option": option,
  }
  print(args)

  return args
