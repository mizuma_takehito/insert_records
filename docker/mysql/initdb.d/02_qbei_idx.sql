USE qbei;

ALTER TABLE Reserve ADD INDEX idx_Reserve_start_datetime (start_datetime);
ALTER TABLE UsedHistory ADD INDEX idx_UsedHistory_loaned_datetime (loaned_datetime);
ALTER TABLE ReturnHistory ADD INDEX idx_ReturnHistory_return_datetime (return_datetime);
ALTER TABLE ReturnHistory ADD INDEX idx_ReturnHistory_payoff_done_datetime (payoff_done_datetime);
ALTER TABLE ConsumedTicket ADD INDEX idx_ConsumedTicket_consumed_datetime (consumed_datetime);
ALTER TABLE Ticket ADD INDEX idx_Ticket_period_date (period_date);
ALTER TABLE DeviceUniqueId ADD INDEX idx_DeviceUniqueId_device_token (device_token);
