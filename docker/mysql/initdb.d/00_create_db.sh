#!/bin/bash -eu

mysql=( mysql -uroot -ppassword )

"${mysql[@]}" <<-EOSQL
CREATE DATABASE IF NOT EXISTS qbei;
GRANT ALL ON qbei.* TO '${MYSQL_USER}'@'%' ;
EOSQL
