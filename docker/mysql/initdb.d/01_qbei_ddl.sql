USE qbei;
CREATE TABLE FaqCategory (
 faq_category_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 name_code VARCHAR(50) NOT NULL,
 display_order INT NOT NULL
);



CREATE TABLE FaqContents (
 faq_category_number INT NOT NULL,
 display_order INT NOT NULL,
 question_name_code VARCHAR(50) NOT NULL,
 answer_name_code VARCHAR(50) NOT NULL
);

ALTER TABLE FaqContents ADD CONSTRAINT PK_FaqContents PRIMARY KEY (faq_category_number,display_order);


CREATE TABLE Notice (
 notice_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 title_name_code VARCHAR(50) NOT NULL,
 notice_name_code VARCHAR(50) NOT NULL,
 registered_datetime DATETIME NOT NULL,
 display_limit_datetime DATETIME NOT NULL
);



CREATE TABLE ApplicationVersion (
 application_kind VARCHAR(20) NOT NULL,
 minimum_major INT NOT NULL,
 minimum_minor INT NOT NULL,
 minimum_revision INT NOT NULL
);

ALTER TABLE ApplicationVersion ADD CONSTRAINT PK_ApplicationVersion PRIMARY KEY (application_kind);


CREATE TABLE CouponTicketPrice (
 application_start_date DATE NOT NULL,
 price INT NOT NULL
);

ALTER TABLE CouponTicketPrice ADD CONSTRAINT PK_CouponTicketPrice PRIMARY KEY (application_start_date);


CREATE TABLE Tenant (
 tenant_code VARCHAR(20) NOT NULL,
 name_code VARCHAR(50) NOT NULL,
 name VARCHAR(255) NOT NULL,
 bank_code VARCHAR(50) NOT NULL,
 bank_name_kana VARCHAR(50) NOT NULL,
 satellite_bank_code VARCHAR(50) NOT NULL,
 satellite_bank_name_kana VARCHAR(50) NOT NULL,
 account_kind_code VARCHAR(10) NOT NULL,
 account_number VARCHAR(20) NOT NULL,
 account_name_kana VARCHAR(100) NOT NULL
);

ALTER TABLE Tenant ADD CONSTRAINT PK_Tenant PRIMARY KEY (tenant_code);


CREATE TABLE Plan (
 plan_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 name_code VARCHAR(50) NOT NULL,
 minutes_unit INT NOT NULL,
 is_reservation BOOLEAN NOT NULL,
 cancelable_minutes INT NOT NULL,
 grace_minutes INT NOT NULL,
 extension_plan_number INT,
 longterm_parking_minutes_threshold INT NOT NULL,
 return_guidance_minutes_threshold INT NOT NULL,
 temporary_parking_minutes_threshold_for_auto_return INT NOT NULL,
 auto_return_available_minutes_before INT NOT NULL,
 auto_return_available_minutes_after INT NOT NULL,
 long_use_notification_interval INT NOT NULL
);



CREATE TABLE MaintenanceStatus (
 maintenance_status_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 name VARCHAR(20) NOT NULL,
 display_order INT NOT NULL
);



CREATE TABLE NameMaster (
 name_code VARCHAR(255) CHARACTER SET ascii NOT NULL,
 language_code VARCHAR(10) NOT NULL,
 name TEXT NOT NULL
);

ALTER TABLE NameMaster ADD CONSTRAINT PK_NameMaster PRIMARY KEY (name_code,language_code);


CREATE TABLE BusinessDisruptionLimitPoint (
 application_start_date DATE NOT NULL,
 daily_limit INT NOT NULL,
 monthly_limit INT NOT NULL
);

ALTER TABLE BusinessDisruptionLimitPoint ADD CONSTRAINT PK_BusinessDisruptionLimitPoint PRIMARY KEY (application_start_date);


CREATE TABLE CountryCode (
 country_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 country_code INT NOT NULL,
 name_code VARCHAR(50) NOT NULL,
 display_order INT NOT NULL
);



CREATE TABLE TimeIntervalEvent (
 time_interval_event_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 kind VARCHAR(20) NOT NULL,
 start_date_time DATETIME NOT NULL,
 end_date_time DATETIME
);



CREATE TABLE Shop (
 shop_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 name_code VARCHAR(50) NOT NULL
);




CREATE TABLE SalesTaxRate (
 application_start_date DATE NOT NULL,
 tax_rate INT NOT NULL
);

ALTER TABLE SalesTaxRate ADD CONSTRAINT PK_SalesTaxRate PRIMARY KEY (application_start_date);


CREATE TABLE State (
 state_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 name_code VARCHAR(50) NOT NULL
);



CREATE TABLE TermCategory (
 term_category_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 name_code VARCHAR(50) NOT NULL,
 display_order INT NOT NULL
);



CREATE TABLE TermContents (
 term_category_number INT NOT NULL,
 display_order INT NOT NULL,
 title_name_code VARCHAR(50) NOT NULL,
 text_name_code VARCHAR(50) NOT NULL
);

ALTER TABLE TermContents ADD CONSTRAINT PK_TermContents PRIMARY KEY (term_category_number,display_order);


CREATE TABLE VehicleType (
 vehicle_type_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 name_code VARCHAR(50) NOT NULL
);



CREATE TABLE PlanByVehicleType (
 vehicle_type_number INT NOT NULL,
 plan_number INT NOT NULL,
 name_code VARCHAR(50) NOT NULL
);

ALTER TABLE PlanByVehicleType ADD CONSTRAINT PK_PlanByVehicleType PRIMARY KEY (vehicle_type_number,plan_number);


CREATE TABLE PlanByVehicleTypeHistory (
 vehicle_type_number INT NOT NULL,
 registered_datetime DATETIME NOT NULL,
 plan_number INT NOT NULL,
 price INT NOT NULL
);

ALTER TABLE PlanByVehicleTypeHistory ADD CONSTRAINT PK_PlanByVehicleTypeHistory PRIMARY KEY (vehicle_type_number,registered_datetime,plan_number);


CREATE TABLE VehicleProvider (
 vehicle_provider_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 tenant_code VARCHAR(20) NOT NULL,
 reduction_ratio DECIMAL(6,3) DEFAULT 0.0 NOT NULL,
 monthly_payment_target_flag BOOLEAN DEFAULT false NOT NULL
);



CREATE TABLE ParkingProvider (
 parking_provider_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 tenant_code VARCHAR(20) NOT NULL
);



CREATE TABLE User (
 user_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 user_id VARCHAR(255) CHARACTER SET ascii UNIQUE NOT NULL,
 email VARCHAR(100) UNIQUE NOT NULL,
 country_number INT NOT NULL,
 phone_number VARCHAR(20) NOT NULL,
 birth_day DATE NOT NULL,
 status TINYINT DEFAULT 1 NOT NULL,
 manor_checked_datetime DATETIME,
 plan_number INT,
 last_access_language_code VARCHAR(10) DEFAULT 'en' NOT NULL,
 registered_datetime DATETIME NOT NULL,
 email_verified BOOLEAN DEFAULT false NOT NULL,
 phone_number_verified BOOLEAN DEFAULT false NOT NULL,
 last_name VARCHAR(255) NOT NULL,
 first_name VARCHAR(255) NOT NULL,
 signup_confirm_target VARCHAR(15) NOT NULL,
 last_login_datetime DATETIME,
 veritrans_id VARCHAR(30) DEFAULT '' NOT NULL
);

ALTER TABLE User ADD UNIQUE UK_User_0 (country_number, phone_number);

CREATE TABLE MissingUserId (
 missing_user_id VARCHAR(255) CHARACTER SET ascii PRIMARY KEY NOT NULL
);


CREATE TABLE DeviceUniqueId (
 device_unique_id_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 user_number INT NOT NULL,
 mobile_type VARCHAR(20) NOT NULL,
 device_token VARCHAR(255) NOT NULL,
 arn VARCHAR(255) NOT NULL
);



CREATE TABLE SettlementMeans (
 settlement_means_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 card_id VARCHAR(50) NOT NULL,
 user_number INT NOT NULL
);



CREATE TABLE Vehicle (
 vehicle_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 vehicle_type_number INT NOT NULL
);



CREATE TABLE Parking (
 parking_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 parking_name_code VARCHAR(50) NOT NULL,
 address_name_code VARCHAR(255) NOT NULL,
 latitude DECIMAL(9,6) NOT NULL,
 longitude DECIMAL(9,6) NOT NULL,
 geohash VARCHAR(10) NOT NULL,
 beacon_id VARCHAR(20),
 parked_possible_count SMALLINT NOT NULL,
 parked_limit_count SMALLINT NOT NULL,
 available_lower_limit_count SMALLINT NOT NULL DEFAULT 1,
 image_count SMALLINT DEFAULT 0 NOT NULL,
 sales_information_name_code VARCHAR(50) NOT NULL,
 contract_type VARCHAR(10) NOT NULL,
 fixed_price INT DEFAULT 0 NOT NULL,
 fluctuation_departure_ratio DECIMAL(6,3) DEFAULT 0.0 NOT NULL,
 fluctuation_return_ratio DECIMAL(6,3) DEFAULT 0.0 NOT NULL,
 monthly_payment_target_flag BOOLEAN DEFAULT false NOT NULL,
 invalid_flag BOOLEAN DEFAULT false NOT NULL,
 parking_provider_number INT NOT NULL,
 pin_image_url VARCHAR(100) NOT NULL,
 site_url VARCHAR(100),
 registered_datetime DATETIME DEFAULT '2021-01-01 00:00:00' NOT NULL
);




CREATE TABLE Ticket (
 ticket_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 acquisition_date DATE NOT NULL,
 period_date DATE NOT NULL,
 price INT NOT NULL,
 user_number INT NOT NULL
);



CREATE TABLE Port (
 parking_number INT NOT NULL,
 shop_number INT
);

ALTER TABLE Port ADD CONSTRAINT PK_Port PRIMARY KEY (parking_number);


CREATE TABLE MaintenanceStartHistory (
 maintenance_start_history_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 start_datetime DATETIME NOT NULL,
 comment TEXT,
 last_modified_person VARCHAR(100) NOT NULL,
 vehicle_number INT NOT NULL,
 maintenance_status_number INT DEFAULT 1 NOT NULL
);



CREATE TABLE VehicleLock (
 vehicle_lock_id VARCHAR(20) NOT NULL,
 vehicle_number INT
);

ALTER TABLE VehicleLock ADD CONSTRAINT PK_VehicleLock PRIMARY KEY (vehicle_lock_id);


CREATE TABLE Reserve (
 reserve_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 start_datetime DATETIME NOT NULL,
 end_datetime DATETIME NOT NULL,
 parking_number INT NOT NULL,
 is_canceled BOOLEAN DEFAULT false NOT NULL
);



CREATE TABLE VehicleState (
 vehicle_number INT NOT NULL,
 state_number INT NOT NULL,
 update_datetime DATETIME NOT NULL,
 parking_number INT
);

ALTER TABLE VehicleState ADD CONSTRAINT PK_VehicleState PRIMARY KEY (vehicle_number);


CREATE TABLE ApplicableVehicleProvider (
 application_start_date DATE NOT NULL,
 vehicle_number INT NOT NULL,
 vehicle_provider_number INT NOT NULL
);

ALTER TABLE ApplicableVehicleProvider ADD CONSTRAINT PK_ApplicableVehicleProvider PRIMARY KEY (application_start_date,vehicle_number);


CREATE TABLE Keep (
 keep_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 end_datetime DATETIME NOT NULL,
 vehicle_number INT NOT NULL,
 user_number INT NOT NULL,
 reserve_number INT,
 secret_pin CHAR(4) NOT NULL,
 vehicle_type_number INT NOT NULL,
 registered_datetime DATETIME NOT NULL,
 plan_number INT NOT NULL,
 vehicle_provider_number INT NOT NULL
);

CREATE TABLE KeepEndNotificationHistory (
 keep_number INT NOT NULL,
 is_user_cancel BOOLEAN DEFAULT false NOT NULL
);

ALTER TABLE KeepEndNotificationHistory ADD CONSTRAINT PK_KeepEndNotificationHistory PRIMARY KEY (keep_number);




CREATE TABLE Coupon (
 coupon_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 coupon_code VARCHAR(10) NOT NULL,
 period_date DATE NOT NULL,
 distribution_unit SMALLINT NOT NULL,
 limit_user INT NOT NULL,
 parking_number INT,
 caption_name_code VARCHAR(50) NOT NULL,
 ticket_period_date DATE
);



CREATE TABLE CouponTicket (
 ticket_number INT NOT NULL,
 coupon_number INT NOT NULL
);

ALTER TABLE CouponTicket ADD CONSTRAINT PK_CouponTicket PRIMARY KEY (ticket_number);


CREATE TABLE MaintenanceEndHistory (
 maintenance_start_history_number INT NOT NULL,
 end_datetime DATETIME NOT NULL,
 last_modified_person VARCHAR(100) NOT NULL
);

ALTER TABLE MaintenanceEndHistory ADD CONSTRAINT PK_MaintenanceEndHistory PRIMARY KEY (maintenance_start_history_number);


CREATE TABLE UsedHistory (
 used_history_number INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
 loaned_datetime DATETIME NOT NULL,
 start_parking_number INT NOT NULL,
 keep_number INT NOT NULL
);



CREATE TABLE UsedHistoryNotificationHistory (
 used_history_number INT NOT NULL,
 grace_count INT NOT NULL,
 notify_kind VARCHAR(20) NOT NULL
);

ALTER TABLE UsedHistoryNotificationHistory ADD CONSTRAINT PK_UsedHistoryNotificationHistory PRIMARY KEY (used_history_number,grace_count);

CREATE TABLE ReturnHistory (
 used_history_number INT NOT NULL,
 return_datetime DATETIME NOT NULL,
 parking_number INT NOT NULL,
 parking_judge_ground VARCHAR(20) DEFAULT 'sp_gps' NOT NULL,
 payoff_done_datetime DATETIME,
 deputy_returning_person VARCHAR(100),
 return_kind VARCHAR(20) NOT NULL,
 usage_amount INT NOT NULL,
 billing_amount INT NOT NULL
);

ALTER TABLE ReturnHistory ADD CONSTRAINT PK_ReturnHistory PRIMARY KEY (used_history_number);


CREATE TABLE TemporaryParkedHistory (
 used_history_number INT NOT NULL,
 lock_unlock_datetime DATETIME NOT NULL,
 lock_unlock_kind VARCHAR(10) NOT NULL,
 parking_number INT,
 parking_judge_ground VARCHAR(20)
);

ALTER TABLE TemporaryParkedHistory ADD CONSTRAINT PK_TemporaryParkedHistory PRIMARY KEY (used_history_number,lock_unlock_datetime);


CREATE TABLE TemporaryParkingNotificationHistory (
 used_history_number INT NOT NULL,
 lock_unlock_datetime DATETIME NOT NULL,
 notify_kind VARCHAR(20) NOT NULL
);

ALTER TABLE TemporaryParkingNotificationHistory ADD CONSTRAINT PK_TemporaryParkingNotificationHistory PRIMARY KEY (used_history_number,lock_unlock_datetime,notify_kind);


CREATE TABLE SettlementHistory (
 used_history_number INT NOT NULL,
 settlement_means_number INT NOT NULL,
 settlement_datetime DATETIME NOT NULL,
 isValid BOOLEAN NOT NULL
);

ALTER TABLE SettlementHistory ADD CONSTRAINT PK_SettlementHistory PRIMARY KEY (used_history_number,settlement_means_number,settlement_datetime);


CREATE TABLE ConsumedTicket (
 ticket_number INT NOT NULL,
 consumed_datetime DATETIME NOT NULL,
 used_history_number INT NOT NULL
);

ALTER TABLE ConsumedTicket ADD CONSTRAINT PK_ConsumedTicket PRIMARY KEY (ticket_number);

CREATE TABLE MonthlySummary (
 target_month DATE NOT NULL,
 summary_start_datetime DATETIME NOT NULL,
 summary_end_datetime DATETIME
);

ALTER TABLE MonthlySummary ADD CONSTRAINT PK_MonthlySummary PRIMARY KEY (target_month);



CREATE TABLE MonthlyStatistics (
 target_month DATE NOT NULL,
 unique_new_users INT NOT NULL,
 unique_repeat_users INT NOT NULL,
 unique_operation_vehicles INT NOT NULL,
 registered_maintenance_vehicles INT NOT NULL,
 registered_operation_vehicles INT NOT NULL,
 all_users INT DEFAULT 0 NOT NULL,
 signup_users INT DEFAULT 0 NOT NULL,
 all_ports INT DEFAULT 0 NOT NULL,
 add_ports INT DEFAULT 0 NOT NULL
);

ALTER TABLE MonthlyStatistics ADD CONSTRAINT PK_MonthlyStatistics PRIMARY KEY (target_month);

CREATE TABLE MonthlyStatisticsForVehicleType (
 target_month DATE NOT NULL,
 vehicle_type_number INT NOT NULL,
 unique_users INT NOT NULL,
 unique_operation_vehicles INT NOT NULL
);

ALTER TABLE MonthlyStatisticsForVehicleType ADD CONSTRAINT PK_MonthlyStatisticsForVehicleType PRIMARY KEY (target_month,vehicle_type_number);

CREATE TABLE SalesSummaryForPlanByVehicleType (
 target_month DATE NOT NULL,
 vehicle_type_number INT NOT NULL,
 plan_number INT NOT NULL,
 plan_price INT NOT NULL,
 number_of_credits INT NOT NULL,
 unique_users INT NOT NULL,
 unique_uses INT NOT NULL,
 unique_operation_vehicles INT NOT NULL,
 usage_amount_summary INT NOT NULL,
 coupon_deduction_amount_summary INT NOT NULL,
 billing_amount_summary INT NOT NULL,
 carryover_amount_summary INT NOT NULL,
 uncollecting_amount_summary INT NOT NULL,
 allocation_amount_summary INT NOT NULL,
 disappearance_amount_summary INT NOT NULL
);

ALTER TABLE SalesSummaryForPlanByVehicleType ADD CONSTRAINT PK_SalesSummaryForPlanByVehicleType PRIMARY KEY (target_month,vehicle_type_number,plan_number);



CREATE TABLE MonthlyUsageStatement (
 target_month DATE NOT NULL,
 used_history_number INT NOT NULL,
 SalesKind VARCHAR(15) NOT NULL,
 usage_amount INT NOT NULL,
 coupon_amount INT NOT NULL,
 billing_amount INT NOT NULL,
 extension_times INT NOT NULL,
 user_number INT NOT NULL,
 vehicle_type_number INT NOT NULL,
 plan_number INT NOT NULL
);

ALTER TABLE MonthlyUsageStatement ADD CONSTRAINT PK_MonthlyUsageStatement PRIMARY KEY (target_month,used_history_number);

CREATE TABLE MonthlyParkingPaymentStatement (
 target_month DATE NOT NULL,
 used_history_number INT NOT NULL,
 useKind VARCHAR(10) NOT NULL,
 parking_number INT NOT NULL,
 base_datetime DATETIME NOT NULL,
 fluctuation_departure_ratio DECIMAL(6,3) NOT NULL,
 fluctuation_return_ratio DECIMAL(6,3) NOT NULL,
 contract_type VARCHAR(10) NOT NULL,
 fixed_price INT NOT NULL,
 parked_possible_count SMALLINT NOT NULL,
 monthly_payment_target_flag BOOLEAN NOT NULL,
 payment_amount INT NOT NULL
);

ALTER TABLE MonthlyParkingPaymentStatement ADD CONSTRAINT PK_MonthlyParkingPaymentStatement PRIMARY KEY (target_month,used_history_number,useKind);





ALTER TABLE FaqContents ADD CONSTRAINT FK_FaqContents_0 FOREIGN KEY (faq_category_number) REFERENCES FaqCategory (faq_category_number);


ALTER TABLE TermContents ADD CONSTRAINT FK_TermContents_0 FOREIGN KEY (term_category_number) REFERENCES TermCategory (term_category_number);



ALTER TABLE PlanByVehicleType ADD CONSTRAINT FK_PlanByVehicleType_0 FOREIGN KEY (vehicle_type_number) REFERENCES VehicleType (vehicle_type_number);
ALTER TABLE PlanByVehicleType ADD CONSTRAINT FK_PlanByVehicleType_1 FOREIGN KEY (plan_number) REFERENCES Plan (plan_number);


ALTER TABLE PlanByVehicleTypeHistory ADD CONSTRAINT FK_PlanByVehicleTypeHistory_0 FOREIGN KEY (vehicle_type_number,plan_number) REFERENCES PlanByVehicleType (vehicle_type_number,plan_number);


ALTER TABLE VehicleProvider ADD CONSTRAINT FK_VehicleProvider_0 FOREIGN KEY (tenant_code) REFERENCES Tenant (tenant_code);


ALTER TABLE ParkingProvider ADD CONSTRAINT FK_ParkingProvider_0 FOREIGN KEY (tenant_code) REFERENCES Tenant (tenant_code);


ALTER TABLE User ADD CONSTRAINT FK_User_0 FOREIGN KEY (country_number) REFERENCES CountryCode (country_number);
ALTER TABLE User ADD CONSTRAINT FK_User_1 FOREIGN KEY (plan_number) REFERENCES Plan (plan_number);




ALTER TABLE DeviceUniqueId ADD CONSTRAINT FK_DeviceUniqueId_0 FOREIGN KEY (user_number) REFERENCES User (user_number);


ALTER TABLE SettlementMeans ADD CONSTRAINT FK_SettlementMeans_0 FOREIGN KEY (user_number) REFERENCES User (user_number);


ALTER TABLE Vehicle ADD CONSTRAINT FK_Vehicle_0 FOREIGN KEY (vehicle_type_number) REFERENCES VehicleType (vehicle_type_number);


ALTER TABLE Parking ADD CONSTRAINT FK_Parking_0 FOREIGN KEY (parking_provider_number) REFERENCES ParkingProvider (parking_provider_number);




ALTER TABLE Ticket ADD CONSTRAINT FK_Ticket_0 FOREIGN KEY (user_number) REFERENCES User (user_number);


ALTER TABLE Port ADD CONSTRAINT FK_Port_0 FOREIGN KEY (parking_number) REFERENCES Parking (parking_number);
ALTER TABLE Port ADD CONSTRAINT FK_Port_1 FOREIGN KEY (shop_number) REFERENCES Shop (shop_number);


ALTER TABLE MaintenanceStartHistory ADD CONSTRAINT FK_MaintenanceStartHistory_0 FOREIGN KEY (vehicle_number) REFERENCES Vehicle (vehicle_number);
ALTER TABLE MaintenanceStartHistory ADD CONSTRAINT FK_MaintenanceStartHistory_1 FOREIGN KEY (maintenance_status_number) REFERENCES MaintenanceStatus (maintenance_status_number);


ALTER TABLE VehicleLock ADD CONSTRAINT FK_VehicleLock_0 FOREIGN KEY (vehicle_number) REFERENCES Vehicle (vehicle_number);


ALTER TABLE Reserve ADD CONSTRAINT FK_Reserve_0 FOREIGN KEY (parking_number) REFERENCES Port (parking_number);


ALTER TABLE VehicleState ADD CONSTRAINT FK_VehicleState_0 FOREIGN KEY (vehicle_number) REFERENCES Vehicle (vehicle_number);
ALTER TABLE VehicleState ADD CONSTRAINT FK_VehicleState_1 FOREIGN KEY (state_number) REFERENCES State (state_number);
ALTER TABLE VehicleState ADD CONSTRAINT FK_VehicleState_2 FOREIGN KEY (parking_number) REFERENCES Port (parking_number);


ALTER TABLE ApplicableVehicleProvider ADD CONSTRAINT FK_ApplicableVehicleProvider_0 FOREIGN KEY (vehicle_number) REFERENCES VehicleState (vehicle_number);
ALTER TABLE ApplicableVehicleProvider ADD CONSTRAINT FK_ApplicableVehicleProvider_1 FOREIGN KEY (vehicle_provider_number) REFERENCES VehicleProvider (vehicle_provider_number);


ALTER TABLE Keep ADD CONSTRAINT FK_Keep_0 FOREIGN KEY (vehicle_number) REFERENCES Vehicle (vehicle_number);
ALTER TABLE Keep ADD CONSTRAINT FK_Keep_1 FOREIGN KEY (user_number) REFERENCES User (user_number);
ALTER TABLE Keep ADD CONSTRAINT FK_Keep_2 FOREIGN KEY (reserve_number) REFERENCES Reserve (reserve_number);
ALTER TABLE Keep ADD CONSTRAINT FK_Keep_3 FOREIGN KEY (vehicle_type_number,registered_datetime,plan_number) REFERENCES PlanByVehicleTypeHistory (vehicle_type_number,registered_datetime,plan_number);
ALTER TABLE Keep ADD CONSTRAINT FK_Keep_4 FOREIGN KEY (vehicle_provider_number) REFERENCES VehicleProvider (vehicle_provider_number);

ALTER TABLE KeepEndNotificationHistory ADD CONSTRAINT FK_KeepEndNotificationHistory_0 FOREIGN KEY (keep_number) REFERENCES Keep (keep_number);

ALTER TABLE Coupon ADD CONSTRAINT FK_Coupon_0 FOREIGN KEY (parking_number) REFERENCES Port (parking_number);


ALTER TABLE CouponTicket ADD CONSTRAINT FK_CouponTicket_0 FOREIGN KEY (ticket_number) REFERENCES Ticket (ticket_number);
ALTER TABLE CouponTicket ADD CONSTRAINT FK_CouponTicket_1 FOREIGN KEY (coupon_number) REFERENCES Coupon (coupon_number);


ALTER TABLE MaintenanceEndHistory ADD CONSTRAINT FK_MaintenanceEndHistory_0 FOREIGN KEY (maintenance_start_history_number) REFERENCES MaintenanceStartHistory (maintenance_start_history_number);


ALTER TABLE UsedHistory ADD CONSTRAINT FK_UsedHistory_0 FOREIGN KEY (start_parking_number) REFERENCES Port (parking_number);
ALTER TABLE UsedHistory ADD CONSTRAINT FK_UsedHistory_1 FOREIGN KEY (keep_number) REFERENCES Keep (keep_number);


ALTER TABLE UsedHistoryNotificationHistory ADD CONSTRAINT FK_UsedHistoryNotificationHistory_0 FOREIGN KEY (used_history_number) REFERENCES UsedHistory (used_history_number);


ALTER TABLE ReturnHistory ADD CONSTRAINT FK_ReturnHistory_0 FOREIGN KEY (used_history_number) REFERENCES UsedHistory (used_history_number);
ALTER TABLE ReturnHistory ADD CONSTRAINT FK_ReturnHistory_1 FOREIGN KEY (parking_number) REFERENCES Parking (parking_number);


ALTER TABLE TemporaryParkedHistory ADD CONSTRAINT FK_TemporaryParkedHistory_0 FOREIGN KEY (used_history_number) REFERENCES UsedHistory (used_history_number);
ALTER TABLE TemporaryParkedHistory ADD CONSTRAINT FK_TemporaryParkedHistory_1 FOREIGN KEY (parking_number) REFERENCES Parking (parking_number);


ALTER TABLE TemporaryParkingNotificationHistory ADD CONSTRAINT FK_TemporaryParkingNotificationHistory_0 FOREIGN KEY (used_history_number,lock_unlock_datetime) REFERENCES TemporaryParkedHistory (used_history_number,lock_unlock_datetime);


ALTER TABLE SettlementHistory ADD CONSTRAINT FK_SettlementHistory_0 FOREIGN KEY (used_history_number) REFERENCES ReturnHistory (used_history_number);
ALTER TABLE SettlementHistory ADD CONSTRAINT FK_SettlementHistory_1 FOREIGN KEY (settlement_means_number) REFERENCES SettlementMeans (settlement_means_number);


ALTER TABLE ConsumedTicket ADD CONSTRAINT FK_ConsumedTicket_0 FOREIGN KEY (ticket_number) REFERENCES Ticket (ticket_number);
ALTER TABLE ConsumedTicket ADD CONSTRAINT FK_ConsumedTicket_1 FOREIGN KEY (used_history_number) REFERENCES ReturnHistory (used_history_number);

ALTER TABLE Plan ADD CONSTRAINT FK_Plan_0  FOREIGN KEY (extension_plan_number) REFERENCES Plan (plan_number);


ALTER TABLE MonthlyStatistics ADD CONSTRAINT FK_MonthlyStatistics_0 FOREIGN KEY (target_month) REFERENCES MonthlySummary (target_month);


ALTER TABLE MonthlyStatisticsForVehicleType ADD CONSTRAINT FK_MonthlyStatisticsForVehicleType_0 FOREIGN KEY (target_month) REFERENCES MonthlyStatistics (target_month);


ALTER TABLE SalesSummaryForPlanByVehicleType ADD CONSTRAINT FK_SalesSummaryForPlanByVehicleType_0 FOREIGN KEY (target_month,vehicle_type_number) REFERENCES MonthlyStatisticsForVehicleType (target_month,vehicle_type_number);





ALTER TABLE MonthlyUsageStatement ADD CONSTRAINT FK_MonthlyUsageStatement_0 FOREIGN KEY (target_month) REFERENCES MonthlySummary (target_month);
ALTER TABLE MonthlyUsageStatement ADD CONSTRAINT FK_MonthlyUsageStatement_1 FOREIGN KEY (used_history_number) REFERENCES ReturnHistory (used_history_number);
ALTER TABLE MonthlyUsageStatement ADD CONSTRAINT FK_MonthlyUsageStatement_2 FOREIGN KEY (user_number) REFERENCES User (user_number);
ALTER TABLE MonthlyUsageStatement ADD CONSTRAINT FK_MonthlyUsageStatement_3 FOREIGN KEY (vehicle_type_number,plan_number) REFERENCES PlanByVehicleType (vehicle_type_number,plan_number);


ALTER TABLE MonthlyParkingPaymentStatement ADD CONSTRAINT FK_MonthlyParkingPaymentStatement_0 FOREIGN KEY (target_month,used_history_number) REFERENCES MonthlyUsageStatement (target_month,used_history_number);
ALTER TABLE MonthlyParkingPaymentStatement ADD CONSTRAINT FK_MonthlyParkingPaymentStatement_1 FOREIGN KEY (parking_number) REFERENCES Parking (parking_number);
