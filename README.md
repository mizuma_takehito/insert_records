# insertRecords.py

**DB(MySQL)へのINSERTコマンドを生成するためのツール。**

---

## 接続先設定ファイルの編集

`insertRecords.ini` ファイルに、以下のようにDBへの接続設定が記述してある。  
環境に合わせて適宜編集しておく。

```
[MySQL_DB]
host=127.0.0.1
port=3306
db=qbei
user=qbei
passwd=qbei55qbei55
charset=utf8
```

---

## 実行方法 (概要)

```
python3 insertRecords.py <mode> [Args]
```

- **`<mode>`  （詳細後述）**  
  **以下から選択。**
  - **mode1**  
    レコード追加に必要なテーブル情報を出力するモード。  
  - **mode2**   
    mode1で出力されたファイルを編集し、その編集されたファイルを元にSQL文を生成するモード。  
    
---

## 実行方法 -「mode1」
**レコード追加に必要なテーブル情報を出力するモード。**   

**mode1の場合の実行方法**
```
python3 insertRecords.py mode1 [OPTION] <TableNames>
```

### 必須パラメータ  

- **`<TableNames>`**  
  以下いずれかの形式で記述する。
  - **`--all`**  
    全てのテーブルに関する情報を出力する。
  - **`<TableNames1> [<TableName2> [<TableName3> ..] ]`**  
    最低限情報出力したいテーブル名を列挙する。  

### \[OPTION] : 任意オプション   
-  **`-o  <filename>`**  
   テーブル情報出力Excelファイルの指定。 (既定値: 'insertRecords.xlsx')
-  **`-d  <filename>`**  
   初期値設定用Excelファイルの指定。  
   mode1で出力されたファイルのうち、各テーブルの最初のレコードに記入されている値を初期値として代入する。

---

## 実行方法 -「mode2」

**mode1で出力されたファイルを編集し、その編集されたファイルを元にSQL文を生成するモード。**  

**mode2の場合の実行方法**
```
python3 insertRecords.py mode2 [OPTION] <TableNames>
```

### 必須パラメータ  

- **`<TableNames>`**  
  以下いずれかの形式で記述する。
  - **`--all`**  
    全てのテーブルに関する情報を、入力したExcelのシート順に出力する。
  - **`<TableNames1> [<TableName2> [<TableName3> ..] ]`**  
    出力するテーブル名(入力したExcelの各シート名)を列挙。  
    このリストに含まれないシートは出力されない。

### \[OPTION] : 任意オプション
-  **`-i  <filename>`**  
   テーブル情報入力Excelファイルの指定。 (既定値: 'insertRecords.xlsx')  
   このファイルに記入されているレコード情報をもとにINSERT文を出力する。
