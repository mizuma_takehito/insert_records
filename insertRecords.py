from lib.src_initArgs import initArgs
from lib.src_modeController import modeController


###################################################

# main
if __name__ == "__main__":
  # 引数整理
  args = initArgs()

  # モード分岐
  modeController(args["mode"], args["option"])
